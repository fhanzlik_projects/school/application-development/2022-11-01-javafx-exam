module vap2022_11_01_javafx_exam {
	requires transitive javafx.graphics;
	requires javafx.controls;
	requires javafx.fxml;
	requires java.xml;

	opens vap2022_11_01_javafx_exam.controllers to javafx.fxml;
	opens vap2022_11_01_javafx_exam.fragments to javafx.fxml;

	exports vap2022_11_01_javafx_exam;
}
