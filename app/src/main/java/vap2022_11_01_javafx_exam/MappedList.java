package vap2022_11_01_javafx_exam;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.TransformationList;

public class MappedList<T, U> extends TransformationList<U, T> {
	private final Function<? super T, ? extends U> mapper;

	public MappedList(ObservableList<? extends T> source, Function<? super T, ? extends U> mapper) {
		super(source);
		this.mapper = mapper;
	}

	@Override public int getSourceIndex(int index) {
		return index;
	}

	@Override public U get(int index) {
		return mapper.apply(getSource().get(index));
	}

	@Override public int size() {
		return getSource().size();
	}

	@Override public int getViewIndex(int index) {
		return index;
	}

	@Override protected void sourceChanged(ListChangeListener.Change<? extends T> c) {
		fireChange(new ListChangeListener.Change<U>(this) {
			@Override public boolean wasAdded() {
				return c.wasAdded();
			}

			@Override public boolean wasRemoved() {
				return c.wasRemoved();
			}

			@Override public boolean wasReplaced() {
				return c.wasReplaced();
			}

			@Override public boolean wasUpdated() {
				return c.wasUpdated();
			}

			@Override public boolean wasPermutated() {
				return c.wasPermutated();
			}

			@Override public int getPermutation(int i) {
				return c.getPermutation(i);
			}

			@Override protected int[] getPermutation() {
				// This method is only called by the superclass methods
				// wasPermutated() and getPermutation(int), which are
				// both overriden by this class. There is no other way
				// this method can be called.
				throw new AssertionError("Unreachable code");
			}

			@Override public List<U> getRemoved() {
				ArrayList<U> res = new ArrayList<>(c.getRemovedSize());
				for (T e : c.getRemoved()) {
					res.add(mapper.apply(e));
				}
				return res;
			}

			@Override public int getFrom() { return c.getFrom(); }

			@Override public int getTo() { return c.getTo(); }

			@Override public boolean next() {
				return c.next();
			}

			@Override public void reset() {
				c.reset();
			}
		});
	}
}
