package vap2022_11_01_javafx_exam;

import java.util.UUID;
import javafx.beans.value.ObservableValue;

public record Subject(UUID id, ObservableValue<String> name) {}
