package vap2022_11_01_javafx_exam;

import java.util.UUID;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ObservableFloatValue;
import javafx.beans.value.ObservableIntegerValue;
import javafx.beans.value.ObservableValue;

public record Mark(UUID id, Subject subject, ObjectProperty<Mark.Value> value, ObjectProperty<Mark.Weight> weight) {
	public static record Value(UUID id, ObservableValue<String> name, ObservableFloatValue value) {}

	public static record Weight(UUID id, ObservableValue<String> name, ObservableIntegerValue value) {}
}
