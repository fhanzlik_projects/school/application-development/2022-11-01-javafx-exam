package vap2022_11_01_javafx_exam;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class ObservableListBinding<T> implements ObservableList<T> {
	private ObservableValue<ObservableList<T>> itemsProperty;
	private ArrayList<InvalidationListener> invalidationListeners = new ArrayList<>();
	private ArrayList<ListChangeListener<? super T>> changeListeners = new ArrayList<>();

	public ObservableListBinding(ObservableValue<ObservableList<T>> itemsProperty) {
		this.itemsProperty = itemsProperty;

		itemsProperty.addListener((observable, oldValue, newValue) -> {
			invalidationListeners.forEach(t -> {
				oldValue.removeListener(t);
				newValue.addListener(t);
				t.invalidated(observable);
			});
			changeListeners.forEach(t -> {
				oldValue.removeListener(t);
				newValue.addListener(t);
			});
		});
	}

	public ObservableValue<ObservableList<T>> itemsProperty() {
		return itemsProperty;
	}

	@Override public int size() {
		return this.itemsProperty.getValue().size();
	}

	@Override public boolean isEmpty() { return this.itemsProperty.getValue().isEmpty(); }

	@Override public boolean contains(Object o) {
		return this.itemsProperty.getValue().contains(o);
	}

	@Override public Iterator<T> iterator() {
		return this.itemsProperty.getValue().iterator();
	}

	@Override public Object[] toArray() {
		return this.itemsProperty.getValue().toArray();
	}

	@Override public <A> A[] toArray(A[] a) {
		return this.itemsProperty.getValue().toArray(a);
	}

	@Override public boolean add(T e) {
		return this.itemsProperty.getValue().add(e);
	}

	@Override public boolean remove(Object o) {
		return this.itemsProperty.getValue().remove(o);
	}

	@Override public boolean containsAll(Collection<?> c) {
		return this.itemsProperty.getValue().containsAll(c);
	}

	@Override public boolean addAll(Collection<? extends T> c) {
		return this.itemsProperty.getValue().addAll(c);
	}

	@Override public boolean addAll(int index, Collection<? extends T> c) {
		return this.itemsProperty.getValue().addAll(c);
	}

	@Override public boolean removeAll(Collection<?> c) {
		return this.itemsProperty.getValue().removeAll(c);
	}

	@Override public boolean retainAll(Collection<?> c) {
		return this.itemsProperty.getValue().removeAll(c);
	}

	@Override public void clear() {
		this.itemsProperty.getValue().clear();
	}

	@Override public T get(int index) {
		return this.itemsProperty.getValue().get(index);
	}

	@Override public T set(int index, T element) {
		return this.itemsProperty.getValue().set(index, element);
	}

	@Override public void add(int index, T element) {
		this.itemsProperty.getValue().add(index, element);
	}

	@Override public T remove(int index) {
		return this.itemsProperty.getValue().remove(index);
	}

	@Override public int indexOf(Object o) {
		return this.itemsProperty.getValue().indexOf(o);
	}

	@Override public int lastIndexOf(Object o) {
		return this.itemsProperty.getValue().lastIndexOf(o);
	}

	@Override public ListIterator<T> listIterator() {
		return this.itemsProperty.getValue().listIterator();
	}

	@Override public ListIterator<T> listIterator(int index) {
		return this.itemsProperty.getValue().listIterator(index);
	}

	@Override public List<T> subList(int fromIndex, int toIndex) {
		return this.itemsProperty.getValue().subList(fromIndex, toIndex);
	}

	@Override public void addListener(InvalidationListener listener) {
		this.invalidationListeners.add(listener);
		this.itemsProperty.getValue().addListener(listener);
	}

	@Override public void removeListener(InvalidationListener listener) {
		this.invalidationListeners.remove(listener);
		this.itemsProperty.getValue().removeListener(listener);
	}

	@Override public void addListener(ListChangeListener<? super T> listener) {
		this.changeListeners.add(listener);
		this.itemsProperty.getValue().addListener(listener);
	}

	@Override public void removeListener(ListChangeListener<? super T> listener) {
		this.changeListeners.remove(listener);
		this.itemsProperty.getValue().removeListener(listener);
	}

	@SuppressWarnings("unchecked") @Override public boolean addAll(T... elements) {
		return this.itemsProperty.getValue().addAll(elements);
	}

	@SuppressWarnings("unchecked") @Override public boolean setAll(T... elements) {
		return this.itemsProperty.getValue().setAll(elements);
	}

	@Override public boolean setAll(Collection<? extends T> col) {
		return this.itemsProperty.getValue().setAll(col);
	}

	@SuppressWarnings("unchecked") @Override public boolean removeAll(T... elements) {
		return this.itemsProperty.getValue().removeAll(elements);
	}

	@SuppressWarnings("unchecked") @Override public boolean retainAll(T... elements) {
		return this.itemsProperty.getValue().retainAll(elements);
	}

	@Override public void remove(int from, int to) {
		this.itemsProperty.getValue().remove(from, to);
	}
}
