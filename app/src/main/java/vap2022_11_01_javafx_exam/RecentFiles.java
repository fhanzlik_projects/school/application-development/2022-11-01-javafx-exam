package vap2022_11_01_javafx_exam;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class RecentFiles {
	private static Path historyFile = Path.of("./recent_files.xml");

	public static RecentFiles empty() {
		return new RecentFiles(FXCollections.observableArrayList());
	}

	public static RecentFiles load() throws IOException, XMLStreamException {
		var entries = FXCollections.<Path>observableArrayList();

		try {
			Utils.readXmlFile(historyFile.toFile(), (reader, event) -> {
				switch (event) {
					case XMLEvent.START_ELEMENT -> {
						switch (reader.getName().getLocalPart()) {
							case "entry" -> {
								entries.add(Path.of(reader.getAttributeValue(null, "path")));
							}
						}
					}
				}
			});
		} catch (FileNotFoundException e) {
			// if there was no history file, just return the empty entry list
		}

		return new RecentFiles(entries);
	}

	private ObservableList<Path> entries;

	private RecentFiles(ObservableList<Path> entries) {
		this.entries = entries;
	}

	private void save() throws IOException, XMLStreamException {
		Utils.writeXmlFile(historyFile.toFile(), "vap2022_11_01_javafx_exam_file_history", writer -> {
			for (Path entry : entries) {
				writer.writeStartElement("entry");
				writer.writeAttribute("path", entry.toString());
				writer.writeEndElement();
			}
		});
	}

	public void addEntry(Path maybeRelative) {
		try {
			var path = maybeRelative.toAbsolutePath();
			// note that we remove and add instead of only adding if it wasn't already
			// there.
			//
			// this is intentional because it effectively bumps the entry to the top which
			// is desired.
			this.entries.remove(path);
			this.entries.add(path);
			this.save();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ObservableList<Path> entries() {
		return this.entries;
	}
}
