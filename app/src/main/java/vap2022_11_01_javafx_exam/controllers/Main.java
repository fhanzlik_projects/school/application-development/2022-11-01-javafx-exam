package vap2022_11_01_javafx_exam.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.stream.IntStream;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.XMLEvent;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.WritableObjectValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;
import vap2022_11_01_javafx_exam.AppLocale;
import vap2022_11_01_javafx_exam.MappedList;
import vap2022_11_01_javafx_exam.Mark;
import vap2022_11_01_javafx_exam.ObservableListBinding;
import vap2022_11_01_javafx_exam.RecentFiles;
import vap2022_11_01_javafx_exam.Subject;
import vap2022_11_01_javafx_exam.Utils;
import vap2022_11_01_javafx_exam.fragments.MarkActionButtons;

public class Main {
	private ExtensionFilter[] extensionFilters;
	private RecentFiles recentFiles = RecentFiles.empty();

	private ObjectProperty<Model> modelProperty = new SimpleObjectProperty<>(new Model());

	@FXML private ResourceBundle resources;

	@FXML private Node root;
	@FXML private Menu menuFileOpenRecent;
	@FXML private Menu menuConfigureLocale;
	@FXML private ComboBox<Subject> viewedSubjectPicker;
	@FXML private ComboBox<Mark.Value> newMarkValue;
	@FXML private ComboBox<Mark.Weight> newMarkWeight;
	@FXML private TableView<Mark> marks;
	@FXML private TableColumn<Mark, MarkActionButtons> marksColumnActionButtons;
	@FXML private TableColumn<Mark, Mark.Value> marksColumnValue;
	@FXML private TableColumn<Mark, Mark.Weight> marksColumnWeight;
	@FXML private Label averageLabelSubject;
	@FXML private Label averageLabelEverything;

	// kind of a hack to keep the mapped array alive
	//
	// because JavaFX doesn't keep strong references to bound lists, we need to keep
	// it artificially alive so that the menu items don't stop being updated as new
	// entries are added
	private MappedList<Path, MenuItem> preventGarbageCollectionRecentFilesMapped;

	@FXML private void initialize() {
		this.extensionFilters = new ExtensionFilter[] {
			new ExtensionFilter(this.resources.getString("extension_filter.project"), "*.vap2022_11_01_javafx_exam"),
			new ExtensionFilter(this.resources.getString("extension_filter.all"), "*.*"), };

		try {
			this.recentFiles = RecentFiles.load();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		preventGarbageCollectionRecentFilesMapped = new MappedList<>(recentFiles.entries(), entry -> {
			var item = new MenuItem(entry.getFileName().toString());
			item.setMnemonicParsing(false);
			item.setOnAction((e) -> {
				try {
					this.open(entry.toFile());
				} catch (IOException | XMLStreamException e1) {
					e1.printStackTrace();
				}
			});
			return item;
		});
		Bindings.bindContent(this.menuFileOpenRecent.getItems(), preventGarbageCollectionRecentFilesMapped);

		this.menuConfigureLocale
			.getItems()
			.setAll(
				Arrays
					.asList(
						new LocaleWithName("Čeština", new Locale("cs", "CZ")),
						new LocaleWithName("English (Great Britain)", new Locale("en", "GB"))
					)
					.stream()
					.map(locale -> {
						var item = new MenuItem(locale.name());
						item.setMnemonicParsing(false);
						item.setOnAction(e -> {
							try {
								Main.this.setLocale(locale.locale());
							} catch (XMLStreamException | IOException e1) {
								e1.printStackTrace();
							}
						});
						return item;
					})
					.toList()
			);

		var viewedSubjectPickerCellFactory = new SimpleCellFactory<Subject>((subject) -> subject.name().getValue());
		this.viewedSubjectPicker.setCellFactory(viewedSubjectPickerCellFactory);
		this.viewedSubjectPicker.setButtonCell(viewedSubjectPickerCellFactory.call(null));
		this.viewedSubjectPicker
			.itemsProperty()
			.bind(Bindings.createObjectBinding(() -> this.modelProperty.get().subjects, this.modelProperty));
		this.viewedSubjectPicker.getSelectionModel().selectFirst();
		this.modelProperty.addListener(_observable -> this.viewedSubjectPicker.getSelectionModel().selectFirst());

		var newMarkValueCellFactory = new SimpleCellFactory<Mark.Value>((value) -> value.name().getValue());
		this.newMarkValue.setCellFactory(newMarkValueCellFactory);
		this.newMarkValue.setButtonCell(newMarkValueCellFactory.call(null));
		this.newMarkValue
			.itemsProperty()
			.bind(Bindings.createObjectBinding(() -> this.modelProperty.get().markValues, this.modelProperty));

		var newMarkWeightCellFactory = new SimpleCellFactory<Mark.Weight>((weight) -> weight.name().getValue());
		this.newMarkWeight.setCellFactory(newMarkWeightCellFactory);
		this.newMarkWeight.setButtonCell(newMarkWeightCellFactory.call(null));
		this.newMarkWeight
			.itemsProperty()
			.bind(Bindings.createObjectBinding(() -> this.modelProperty.get().markWeights, this.modelProperty));

		var modelMarks = new ObservableListBinding<>(
			Bindings.createObjectBinding(() -> modelProperty.get().marks, modelProperty)
		);

		// since we need to observe an `ObservableList` (`marks`) nested in an
		// `ObservableValue` (`modelProperty`), and this is not directly supported by
		// JavaFX, we need to work around it by using an "intermediate" `ObservableList`
		// initially bound to the current model's marks and rebinding it whenever the
		// model changes.
		var marksFiltered = new FilteredList<>(modelMarks);
		marksFiltered
			.predicateProperty()
			.bind(
				Bindings
					.createObjectBinding(
						() -> mark -> mark.subject().equals(viewedSubjectPicker.getValue()),
						viewedSubjectPicker.valueProperty(),
						this.modelProperty
					)
			);

		this.marks.itemsProperty().bind(new SimpleObjectProperty<>(marksFiltered));
		this.marks.setEditable(true);

		this.marksColumnActionButtons
			.setCellValueFactory(
				p -> Bindings.createObjectBinding(() -> new MarkActionButtons(new MarkActionButtons.Handler() {
					@Override public void delete() {
						Main.this.modelProperty.get().marks.remove(p.getValue());
					}
				}, resources), this.modelProperty)
			);
		this.marksColumnValue
			.cellFactoryProperty()
			.bind(
				Bindings
					.createObjectBinding(
						() -> ComboBoxTableCell
							.forTableColumn(
								Utils.oneWayToStringConverter(v -> v.name().getValue()),
								this.modelProperty.get().markValues
							),
						this.modelProperty
					)
			);
		this.marksColumnValue.setCellValueFactory(p -> p.getValue().value());
		this.marksColumnValue
			.setOnEditCommit(
				e -> e.getTableView().getItems().get(e.getTablePosition().getRow()).value().set(e.getNewValue())
			);

		this.marksColumnWeight
			.cellFactoryProperty()
			.bind(
				Bindings
					.createObjectBinding(
						() -> ComboBoxTableCell
							.forTableColumn(
								Utils.oneWayToStringConverter(w -> w.name().getValue()),
								this.modelProperty.get().markWeights
							),
						this.modelProperty
					)
			);
		this.marksColumnWeight.setCellValueFactory(p -> p.getValue().weight());
		this.marksColumnWeight
			.setOnEditCommit(
				e -> e.getTableView().getItems().get(e.getTablePosition().getRow()).weight().set(e.getNewValue())
			);

		averageLabelSubject
			.textProperty()
			.bind(Bindings.createObjectBinding(() -> Utils.formatMarkAverage(marksFiltered), marksFiltered));
		averageLabelEverything
			.textProperty()
			.bind(Bindings.createObjectBinding(() -> Utils.formatMarkAverage(modelMarks), modelMarks));
	}

	@FXML private void addMark() {
		var subject = this.viewedSubjectPicker.getValue();
		var value = this.newMarkValue.getValue();
		var weight = this.newMarkWeight.getValue();

		if (subject == null || value == null || weight == null) return;

		this.modelProperty.get().marks
			.add(
				new Mark(
					UUID.randomUUID(),
					subject,
					new SimpleObjectProperty<>(value),
					new SimpleObjectProperty<>(weight)
				)
			);
	}

	@FXML private void menuFileNew() {
		this.modelProperty.set(new Model());
	}

	@FXML void menuFileOpen() throws XMLStreamException, IOException {
		var fileChooser = new FileChooser();
		fileChooser.setTitle(this.resources.getString("menu.file.open.dialog.title"));
		fileChooser.getExtensionFilters().addAll(this.extensionFilters);
		var file = fileChooser.showOpenDialog(root.getScene().getWindow());

		if (file == null) { return; }

		this.open(file);
	}

	@FXML void menuFileSave() throws XMLStreamException, IOException {
		var file = this.modelProperty.get().file;
		if (file.get() == null) {
			this.menuFileSaveAs();
		} else {
			this.save();
		}
	}

	@FXML void menuFileSaveAs() throws IOException, XMLStreamException {
		var newFile = askForSaveDestination();
		if (newFile == null) return;
		this.modelProperty.get().file.set(newFile);
		this.save();
	}

	@FXML void menuFileQuit() {
		this.root.getScene().getWindow().hide();
	}

	@FXML void menuConfigureSubjects() {
		var alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(this.resources.getString("menu.configure.subjects.todo_dialog.title"));
		alert.setHeaderText(this.resources.getString("menu.configure.subjects.todo_dialog.header"));
		alert.setContentText(this.resources.getString("menu.configure.subjects.todo_dialog.content"));
		alert.showAndWait();
	}

	@FXML void menuConfigureMarkValues() {
		var alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(this.resources.getString("menu.configure.mark_values.todo_dialog.title"));
		alert.setHeaderText(this.resources.getString("menu.configure.mark_values.todo_dialog.header"));
		alert.setContentText(this.resources.getString("menu.configure.mark_values.todo_dialog.content"));
		alert.showAndWait();
	}

	@FXML void menuConfigureMarkWeights() {
		var alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(this.resources.getString("menu.configure.mark_weights.todo_dialog.title"));
		alert.setHeaderText(this.resources.getString("menu.configure.mark_weights.todo_dialog.header"));
		alert.setContentText(this.resources.getString("menu.configure.mark_weights.todo_dialog.content"));
		alert.showAndWait();
	}

	private void setLocale(Locale locale) throws XMLStreamException, IOException {
		var alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(this.resources.getString("menu.configure.locale.dialog.title"));
		alert.setContentText(this.resources.getString("menu.configure.locale.dialog.content"));
		alert.showAndWait();

		AppLocale.set(locale);
	}

	private File askForSaveDestination() {
		var fileChooser = new FileChooser();
		fileChooser.setTitle(this.resources.getString("menu.file.save.dialog.title"));
		fileChooser.setInitialFileName("project.vap2022_11_01_javafx_exam");
		fileChooser.getExtensionFilters().addAll(this.extensionFilters);
		return fileChooser.showSaveDialog(root.getScene().getWindow());
	}

	private void save() throws IOException, XMLStreamException {
		var file = this.modelProperty.get().file;

		Utils.writeXmlFile(file.get(), "vap2022_11_01_javafx_exam_file", writer -> {
			this.modelProperty.get().writeTo(writer);
		});
		this.recentFiles.addEntry(file.get().toPath());
	}

	private void open(File file) throws IOException, XMLStreamException {
		this.modelProperty.set(Model.readFrom(file));
		this.recentFiles.addEntry(file.toPath());
	}
}

class SimpleCellFactory<T> implements Callback<ListView<T>, ListCell<T>> {
	private Callback<T, String> stringifyItem;

	public SimpleCellFactory(Callback<T, String> stringifyItem) {
		this.stringifyItem = stringifyItem;
	}

	@Override public ListCell<T> call(ListView<T> _listView) {
		return new ListCell<>() {
			protected void updateItem(T item, boolean empty) {
				super.updateItem(item, empty);
				if (item == null || empty) {
					setGraphic(null);
				} else {
					setText(SimpleCellFactory.this.stringifyItem.call(item));
				}
			};
		};
	}
}

class Model {
	public WritableObjectValue<File> file = new SimpleObjectProperty<>(null);

	public ObservableList<Subject> subjects = FXCollections
		.observableList(
			new ArrayList<>(
				Arrays
					.asList(
						new Subject(UUID.randomUUID(), new SimpleObjectProperty<>("Matematika")),
						new Subject(UUID.randomUUID(), new SimpleObjectProperty<>("Vývoj Aplikací")),
						new Subject(UUID.randomUUID(), new SimpleObjectProperty<>("Webové Aplikace")),
						new Subject(UUID.randomUUID(), new SimpleObjectProperty<>("Algoritmizace")),
						new Subject(UUID.randomUUID(), new SimpleObjectProperty<>("IoT")),
						new Subject(UUID.randomUUID(), new SimpleObjectProperty<>("Fyzika")),
						new Subject(UUID.randomUUID(), new SimpleObjectProperty<>("Tělesná Výchova"))
					)
			),
			subject -> new Observable[] { subject.name() }
		);
	public ObservableList<Mark.Value> markValues = FXCollections
		.observableList(
			new ArrayList<>(
				Arrays
					.asList(
						new Mark.Value(UUID.randomUUID(), new SimpleObjectProperty<>("1"), new SimpleFloatProperty(1f)),
						new Mark.Value(
							UUID.randomUUID(),
							new SimpleObjectProperty<>("1-"),
							new SimpleFloatProperty(1.5f)
						),
						new Mark.Value(UUID.randomUUID(), new SimpleObjectProperty<>("2"), new SimpleFloatProperty(2f)),
						new Mark.Value(
							UUID.randomUUID(),
							new SimpleObjectProperty<>("2-"),
							new SimpleFloatProperty(2.5f)
						),
						new Mark.Value(UUID.randomUUID(), new SimpleObjectProperty<>("3"), new SimpleFloatProperty(3f)),
						new Mark.Value(
							UUID.randomUUID(),
							new SimpleObjectProperty<>("3-"),
							new SimpleFloatProperty(3.5f)
						),
						new Mark.Value(UUID.randomUUID(), new SimpleObjectProperty<>("4"), new SimpleFloatProperty(4f)),
						new Mark.Value(
							UUID.randomUUID(),
							new SimpleObjectProperty<>("4-"),
							new SimpleFloatProperty(4.5f)
						),
						new Mark.Value(UUID.randomUUID(), new SimpleObjectProperty<>("5"), new SimpleFloatProperty(5f))
					)
			),
			value -> new Observable[] { value.name(), value.value() }
		);
	public ObservableList<Mark.Weight> markWeights = FXCollections
		.observableList(
			new ArrayList<>(
				IntStream
					.range(1, 11)
					.mapToObj(
						value -> new Mark.Weight(
							UUID.randomUUID(),
							new SimpleObjectProperty<>(String.valueOf(value)),
							new SimpleIntegerProperty(value)
						)
					)
					.toList()
			),
			weight -> new Observable[] { weight.name(), weight.value() }
		);
	public ObservableList<
		Mark> marks = FXCollections.observableArrayList(mark -> new Observable[] { mark.value(), mark.weight() });

	public void writeTo(XMLStreamWriter writer) throws XMLStreamException {
		writer.writeStartElement("subjects");
		for (Subject subject : this.subjects) {
			writer.writeStartElement("subject");
			writer.writeAttribute("id", subject.id().toString());
			writer.writeAttribute("name", subject.name().getValue());
			writer.writeEndElement();
		}
		writer.writeEndElement();

		writer.writeStartElement("mark_values");
		for (var markValue : this.markValues) {
			writer.writeStartElement("mark_value");
			writer.writeAttribute("id", markValue.id().toString());
			writer.writeAttribute("name", markValue.name().getValue());
			writer.writeAttribute("value", String.valueOf(markValue.value().get()));
			writer.writeEndElement();
		}
		writer.writeEndElement();

		writer.writeStartElement("mark_weights");
		for (var markWeight : this.markWeights) {
			writer.writeStartElement("mark_weight");
			writer.writeAttribute("id", markWeight.id().toString());
			writer.writeAttribute("name", markWeight.name().getValue());
			writer.writeAttribute("value", String.valueOf(markWeight.value().get()));
			writer.writeEndElement();
		}
		writer.writeEndElement();

		writer.writeStartElement("marks");
		for (Mark mark : this.marks) {
			writer.writeStartElement("mark");
			writer.writeAttribute("id", mark.id().toString());
			writer.writeAttribute("subject", mark.subject().id().toString());
			writer.writeAttribute("value", mark.value().get().id().toString());
			writer.writeAttribute("weight", mark.weight().get().id().toString());
			writer.writeEndElement();
		}
		writer.writeEndElement();
	}

	public static Model readFrom(File file) throws XMLStreamException, IOException {
		var model = new Model();
		model.file.set(file);

		Utils.readXmlFile(file, (reader, event) -> {
			switch (event) {
				case XMLEvent.START_ELEMENT -> {
					switch (reader.getName().getLocalPart()) {
						case "subjects" -> {
							model.subjects.clear();
						}
						case "subject" -> {
							model.subjects
								.add(
									new Subject(
										UUID.fromString(reader.getAttributeValue(null, "id")),
										new SimpleStringProperty(reader.getAttributeValue(null, "name"))
									)
								);
						}
						case "mark_values" -> {
							model.markValues.clear();
						}
						case "mark_value" -> {
							model.markValues
								.add(
									new Mark.Value(
										UUID.fromString(reader.getAttributeValue(null, "id")),
										new SimpleStringProperty(reader.getAttributeValue(null, "name")),
										new SimpleFloatProperty(
											Float.parseFloat(reader.getAttributeValue(null, "value"))
										)
									)
								);
						}
						case "mark_weights" -> {
							model.markWeights.clear();
						}
						case "mark_weight" -> {
							model.markWeights
								.add(
									new Mark.Weight(
										UUID.fromString(reader.getAttributeValue(null, "id")),
										new SimpleStringProperty(reader.getAttributeValue(null, "name")),
										new SimpleIntegerProperty(
											Integer.parseInt(reader.getAttributeValue(null, "value"))
										)
									)
								);
						}
						case "marks" -> {
							model.marks.clear();
						}
						case "mark" -> {
							var subjectId = UUID.fromString(reader.getAttributeValue(null, "subject"));
							var valueId = UUID.fromString(reader.getAttributeValue(null, "value"));
							var weightId = UUID.fromString(reader.getAttributeValue(null, "weight"));
							model.marks
								.add(
									new Mark(
										UUID.fromString(reader.getAttributeValue(null, "id")),
										model.subjects.stream().filter((s) -> s.id().equals(subjectId)).findAny().get(),
										new SimpleObjectProperty<>(
											model.markValues
												.stream()
												.filter(v -> v.id().equals(valueId))
												.findAny()
												.get()
										),
										new SimpleObjectProperty<>(
											model.markWeights
												.stream()
												.filter(w -> w.id().equals(weightId))
												.findAny()
												.get()
										)
									)
								);
						}
					}
				}
				case XMLEvent.END_ELEMENT -> {
					switch (reader.getName().getLocalPart()) {
						// yes, there are probably better ways to do this, but for now I'm just assuming
						// that this element is the last one we are interested in.
						case "marks" -> {
							break;
						}
					}
				}
			}
		});

		return model;
	}
}

record LocaleWithName(String name, Locale locale) {}
