package vap2022_11_01_javafx_exam.fragments;

import java.io.IOException;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;

public class MarkActionButtons extends HBox {
	public static interface Handler {
		void delete();
	}

	private final Handler handler;

	public MarkActionButtons(Handler handler, ResourceBundle resources) {
		super();

		this.handler = handler;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
				"mark_action_buttons.fxml"));
		fxmlLoader.setResources(resources);
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@FXML
	protected void delete() {
		this.handler.delete();
	}
}
