package vap2022_11_01_javafx_exam;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Objects;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class AppLocale {
	private static final Path localeFile = Path.of("./app_locale.xml");

	public static void set(Locale locale) throws XMLStreamException, IOException {
		Utils.writeXmlFile(localeFile.toFile(), "vap2022_11_01_javafx_exam_locale", writer -> {
			writer.writeAttribute("language", locale.getLanguage());
			writer.writeAttribute("region", locale.getCountry());
		});
	}

	public static Locale read() throws IOException, XMLStreamException {
		// because Java doesn't like us capturing mutable variables in lambdas, we box
		// the variable and mutate it through the box.
		var localeBox = new Object() {
			public Locale locale;
		};

		try {
			Utils.readXmlFile(localeFile.toFile(), (reader, event) -> {
				switch (event) {
					case XMLEvent.START_ELEMENT -> {
						switch (reader.getName().getLocalPart()) {
							case "vap2022_11_01_javafx_exam_locale" -> {
								localeBox.locale = new Locale(
									Objects.requireNonNull(reader.getAttributeValue(null, "language")),
									Objects.requireNonNull(reader.getAttributeValue(null, "region"))
								);
							}
						}
					}
				}
			});
		} catch (FileNotFoundException e) {
			// if there was no history file, just return the empty entry list
		}

		return localeBox.locale;
	}
}
