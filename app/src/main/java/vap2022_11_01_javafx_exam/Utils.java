package vap2022_11_01_javafx_exam;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.stream.Collector;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class Utils {
	public static <T> Collector<T, ?, Optional<Double>> averagingWeighted(
		ToDoubleFunction<T> valueFunction,
		ToIntFunction<T> weightFunction
	) {
		class Acc {
			double valueSum = 0;
			long weightSum = 0;
		}
		return Collector.of(Acc::new, (acc, item) -> {
			acc.valueSum += valueFunction.applyAsDouble(item) * weightFunction.applyAsInt(item);
			acc.weightSum += weightFunction.applyAsInt(item);
		}, (acc1, acc2) -> {
			acc1.valueSum += acc2.valueSum;
			acc1.weightSum += acc2.weightSum;
			return acc1;
		},
			acc -> acc.weightSum == 0 ? Optional.empty() : Optional.of(acc.valueSum / acc.weightSum),
			Collector.Characteristics.UNORDERED
		);
	}

	public static String formatMarkAverage(Collection<Mark> marks) {
		return marks
			.stream()
			.collect(
				Utils
					.averagingWeighted(
						mark -> mark.value().get().value().get(),
						mark -> mark.weight().get().value().get()
					)
			)
			.map(a -> String.format("%.2f", a))
			.orElse("n/a");
	}

	public static <T> StringConverter<T> oneWayToStringConverter(Callback<T, String> cb) {
		return new StringConverter<T>() {
			@Override public T fromString(String string) {
				throw new UnsupportedOperationException(
					"attempted to use a `fromString` on `StringConverter` that only supports conversions to string."
				);
			}

			@Override public String toString(T object) {
				return cb.call(object);
			}
		};
	}

	@FunctionalInterface public static interface XmlWriteCallback {
		void write(XMLStreamWriter writer) throws XMLStreamException;
	}

	public static void writeXmlFile(
		File file,
		String rootTagName,
		XmlWriteCallback callback
	) throws XMLStreamException, IOException {
		try (var out = new BufferedOutputStream(new FileOutputStream(file))) {
			var writer = XMLOutputFactory.newInstance().createXMLStreamWriter(out);

			writer.writeStartDocument("utf-8", "1.0");
			writer.writeStartElement(rootTagName);

			callback.write(writer);

			writer.writeEndElement();
			writer.writeEndDocument();

			writer.flush();
			writer.close();
		}
	}

	public static void readXmlFile(
		File file,
		BiConsumer<XMLStreamReader, Integer> eventConsumer
	) throws IOException, XMLStreamException {
		try (var in = new BufferedInputStream(new FileInputStream(file))) {
			var reader = XMLInputFactory.newInstance().createXMLStreamReader(in);

			while (reader.hasNext()) {
				eventConsumer.accept(reader, reader.next());
			}
		}
	}
}
