{
	description = "recourse";

	inputs = {
		flake-compat = { url = "github:edolstra/flake-compat"; flake = false; };
		nixpkgs      = { url = "github:nixos/nixpkgs/nixos-unstable";         };
		flake-utils  = { url = "github:numtide/flake-utils";                  };
	};

	outputs = { self, nixpkgs, flake-utils, ... }:
		flake-utils.lib.eachDefaultSystem (system:
			let
				pkgs = import nixpkgs { inherit system; };
			in rec {
				devShells.default = pkgs.mkShell rec {
					nativeBuildInputs = with pkgs; [
						gradle
						jdk
						# scenebuilder
						jetbrains.idea-community
					];
					buildInputs = with pkgs; [ gtk3 ];
					shellHook = ''
						export XDG_DATA_DIRS="$GSETTINGS_SCHEMAS_PATH"
					'';
				};
			}
		);
}
